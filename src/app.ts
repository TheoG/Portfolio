import * as express from 'express';
import * as exphbs from 'express-handlebars';
import * as fs from 'fs';
import * as session from 'express-session';

const app = express();

app.use("/css", express.static(__dirname + '/public/css'));
app.use("/images", express.static(__dirname + '/public/images'));
app.use("/js", express.static(__dirname + '/public/js'));
app.use("/pdfs", express.static(__dirname + '/public/pdfs'));

app.engine('hbs', exphbs({
	defaultLayout: 'main',
	extname: '.hbs',
	layoutsDir: 'src/views/layouts',
	partialsDir: 'src/views/partials'
}));

app.use(session({
	language : "fr",
	secret: "421337_ftC++%$@$)*()",
	resave: false,
	saveUninitialized: false
}));

app.set('view engine', '.hbs');
app.set('views', __dirname + '/views/');

app.all('*', (req, res, next) => {
	if (typeof req.session.language === 'undefined' || !req.session.language) {
		req.session.language = "fr";
	}
	next();
});

require(__dirname + '/router/router')(app)

app.set("projects", JSON.parse(fs.readFileSync("./projects.json", 'utf-8')));
app.set("personnalProjects", JSON.parse(fs.readFileSync("./personnalProjects.json", 'utf-8')));

app.listen(8080, function () {
	console.log("Ready at http://localhost:8080/");
});
