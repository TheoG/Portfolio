import { HomePage, GetProjects, SetLanguage, ErrorNotFound } from '../controllers/ProjectController'

module.exports = function(app) {
	app.get('/', HomePage);
	app.get('/projects', GetProjects);
	app.get('/projects/', GetProjects);
	app.get('/:language', SetLanguage);

	app.get('*', ErrorNotFound);
}
 