$(document).ready(() => {
	updateDisplayedProjects();

	$(".project-category").click(function() {
		if ($(this).hasClass('category-selected')) {
			$(this).removeClass('category-selected');
		} else {
			$(this).addClass('category-selected');
		}
		updateDisplayedProjects();
	});
	
	$(".project-techno").click(function() {
		if ($(this).hasClass('category-selected')) {
			$(this).removeClass('category-selected');
		} else {
			$(this).addClass('category-selected');
		}
		updateDisplayedProjects();
	});
});

function updateDisplayedProjects() {
	let selectedCategories = Object.values($(".project-category.category-selected").map(function(idx, elem) {
		return this.dataset.categoryName;
	}));
	let selectedTechnos = Object.values($(".project-techno.category-selected").map(function(idx, elem) {
		return this.dataset.categoryName;
	}));

	$('.project').each(function(idx, elem) {
		let type = $(elem).data().projectType;
		let technos = ($(elem).data().projectTechnos).split(',');

		if (selectedCategories.includes(type) && technos.every(function(e) { return selectedTechnos.includes(e) })) {
			$(elem).show();
		} else {
			$(elem).hide();
		}
	});

}