let modal;
let modalClose;
let modalOpened = false;
let projectsDescriptions = [];

$(document).ready(() => {

	modal = $('#projectModal');
	modalClose = $('#closeModal');

	$('.project').click((obj) => {
		// modal.show();
		openModal(obj.currentTarget);
	});
	
	$(window).click(event => {
		// TODO: handle here, if click elsewhere, close modal
		if (modalOpened && !event.target.closest('.modal-content')) {
			closeModal();
		}
	});
	
	$.get("projects", function(data, status) {
		projectsDescriptions = data;
	});

	modalClose.click(() => {
		closeModal();
	});
});

$(document).keydown(function(e) {
	if (e.keyCode === 27 && modalOpened) {
		closeModal();
	}
});

function openModal(projectNode) {
	modal.removeClass('modal-closed');
	modal.addClass('modal-opened');

	$(".modal-content").html("<p>Pas de description pour le moment...</p>");

	for (let i in projectsDescriptions) {
		if ($(projectNode).data().descriptionName == projectsDescriptions[i].project) {
			$(".modal-content").html(projectsDescriptions[i].html);
		}
	}

	setTimeout(() => {
		modalOpened = true;
	}, 200);
}

function closeModal() {
	modalOpened = false;
	// modal.hide();
	modal.removeClass('modal-opened');
	setTimeout(() => {
		modal.addClass('modal-closed');
		$(".modal-content").html("");
	}, 200);
}