import * as hbs from "express-handlebars"
import * as fs from "fs"
import * as path from "path"
import { request } from "http";
import { log } from "util";

export function ErrorNotFound(request, response) {
	
	if (request.session.language == 'fr') {
		response.errorMessage = "Page non trouvée :(";
	} else {
		response.errorMessage = "Page not found :(";
	}
	
	response.render('404', response);
}

export function HomePage(request, response) {
	response.projects = request.app.get('projects');
	response.personnalProjects = request.app.get('personnalProjects');
	
	response.projects.sort((a, b) => {
		let bSplited = b["date"].split("/");
		let aSplited = a["date"].split("/");
		return new Date(bSplited[2], bSplited[1], bSplited[0]).getTime() - new Date(aSplited[2], aSplited[1], aSplited[0]).getTime();
	});

	let projectTypes = response.projects.map(p => p.type);
	let projectTechnos = [].concat(...(response.projects.map(p => p.technos)));
	projectTechnos = projectTechnos.sort();

	let personnalProjectTypes = response.personnalProjects.map(p => p.type);
	let personnalProjectTechnos = [].concat(...(response.personnalProjects.map(p => p.technos)));

	let numberProjectPerType = {};
	let numberProjectPerTechno = {};

	projectTypes.forEach(x => {
		numberProjectPerType[x] = (numberProjectPerType[x] || 0) + 1;
	});

	projectTechnos.forEach(x => {
		numberProjectPerTechno[x] = (numberProjectPerTechno[x] || 0) + 1;
	});
	
	personnalProjectTypes.forEach(x => {
		numberProjectPerType[x] = (numberProjectPerType[x] || 0) + 1;
	});
	
	personnalProjectTechnos.forEach(x => {
		numberProjectPerTechno[x] = (numberProjectPerTechno[x] || 0) + 1;
	});

	let sortedProjectsTypes = [];
	for (let key in numberProjectPerType) {
		sortedProjectsTypes.push([key, numberProjectPerType[key]]);
	}

	sortedProjectsTypes.sort((a, b) => {
		return b[1] - a[1];
	});

	let sortedProjectsTechnos = [];
	for (let key in numberProjectPerTechno) {
		sortedProjectsTechnos.push([key, numberProjectPerTechno[key]]);
	}

	sortedProjectsTechnos.sort((a, b) => {
		return b[1] - a[1];
	});

	// TODO: Need to create mutliple types project (eg Android AND web)
	response.projectsTypes = sortedProjectsTypes;
	response.projectsTechnos = sortedProjectsTechnos;

	response.render("home", response);
}

export function GetProjects(request, response) {
	const hb = hbs.create();
	const folderName = "src/views/partials/projectsDescriptions";

	let projectsFilesNames = [];	
	fs.readdirSync(folderName).forEach(file => {
		if (file.endsWith(request.session.language + ".hbs")) {
			projectsFilesNames.push(file);
		}
	});

	let res = Promise.all(projectsFilesNames.map(fileName => {
		return new Promise(resolve => hb.render(path.join(folderName, fileName), {}).then(renderedHtml => {
			resolve(renderedHtml);
		}));
	}));
	
	let projectsDescriptions = [];
	res.then(code => {
		code.forEach((code, i) => {
			projectsDescriptions.push({
				project: projectsFilesNames[i].substr(0, projectsFilesNames[i].lastIndexOf('_')),
				html: code
			});
		});
		response.json(projectsDescriptions);
	});
}

export function SetLanguage(req, res) {
	if (req.params.language == "en") {
		req.session.language = "en";
	} else {
		req.session.language = "fr";
	}
	HomePage(req, res);
}